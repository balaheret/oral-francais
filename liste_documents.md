Documents à avoir pour le bac de Français
=========================================

D’après le descriptif :

 + Les textes étudiés des œuvres complètes (deux exemplaires)

 + Les textes non annotés des œuvres complémentaires

Séquence 1
----------

| *Le Dormeur du val*, Rimbaud | Les représentations des **Gamins de Paris** |
| *À mademoiselle*, Musset     | Sélection de tableau sur la **pomme**       |
| *Le Crapaud*, V. Hugo        |                                             |

Séquence 2
----------

| *La Princesse de Clèves*, de la Fayette |
| *Manon Lescault*                        |

***À CONTINUER***
